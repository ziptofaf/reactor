import React, { Component } from 'react';
import { connect } from 'react-redux';
import {doChangeUserSessionToken} from '../../actions_creators/userSession';
import { Redirect } from 'react-router'

//simple component that takes care of removing the token. I am actually not sure how to connect
//React to something that would just be a clickable function in a different way

class Signout extends Component {

  render(){
    if (this.props.user_session.token == null) {
      return (
      <Redirect to={"/"}/>
      );
    }
    else {
      return null;
    }
  }

  componentDidMount() {
    this.props.onTokenChange(null);
  }
}

function mapStateToProps(state) {
  return {
    user_session: state.userSessionState,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onTokenChange: (token) => dispatch(doChangeUserSessionToken(token)),
  };
}

const ConnectedSignout = connect(mapStateToProps, mapDispatchToProps)(Signout);

export default ConnectedSignout;
