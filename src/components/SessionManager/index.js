import { Component } from 'react';
import fetch from 'isomorphic-fetch';
import { connect } from 'react-redux';
import {doChangeUserSessionToken} from '../../actions_creators/userSession';
import MAIN_PATH from '../../constants/urls';


//purpose of this component is to automatically refresh tokens when needed and
//remove one from local storage if it's already expired

class SessionManager extends Component {
  dealWithUserToken = () => {
    const current_date = new Date();
    const expire_date = new Date(this.props.user_session.expires_at);
    if (this.props.user_session.token === null) {
      return null;
    }
    if (this.props.user_session.token !== null && ((expire_date - current_date) < 0)) {
      this.props.onTokenChange(null);
      return null;
    }
    if ((expire_date - current_date) < 300000) { //every 5 minutes
      console.log('attempting to refresh');
      const token = this.props.user_session.token;
      const PATH_BASE = MAIN_PATH + 'refresh-token/';
      const FORM_PARAMS = {'token': token};
      fetch(PATH_BASE, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(FORM_PARAMS),
      })
    .then(response => response.json())
    .then(result => this.refreshToken(result))
    .catch(e => this.handleErrors(e));
    }

  }
  refreshToken = (django_response) => {
    const keys = Object.keys(django_response);
    if (keys.includes('token')) {
      this.props.onTokenChange(django_response['token']);
    }
  }

  handleErrors = (e) => {
    console.log(e);
    //om nom nom
    //i eat errors
  }

  render(){
    return null;
  }

  componentDidMount() {
    this.interval = setInterval(this.dealWithUserToken, 25000);
    this.dealWithUserToken();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }
}

function mapStateToProps(state) {
  return {
    user_session: state.userSessionState,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onTokenChange: (token) => dispatch(doChangeUserSessionToken(token)),
  };
}

const ConnectedSessionManager = connect(mapStateToProps, mapDispatchToProps)(SessionManager);

export default ConnectedSessionManager;
