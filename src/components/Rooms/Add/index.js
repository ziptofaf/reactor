import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Checkbox, Form, Message, Progress, Icon, Table } from 'semantic-ui-react';
import fetchFunction from '../../../helper_functions/fetcher';

import { Dimmer, Loader, Segment } from 'semantic-ui-react';
import {doChangeGamemodels} from '../../../actions_creators/gamemodelsList';


class RoomsAdder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gameModels: this.props.gamemodels.gamemodels,
      chosenGameModel: '--Choose One--',
      description: '',
      name: '',
      isPrivate: false,
      maxPlayers: 4,
      created: false
    };
  }

  getAvailableGameSystems = () => {
    if (this.state.gameModels.length === 0) {
      fetchFunction('gamemodels/', this.onSuccessfulSystemsSearch, this.onFailedSystemsSearch);
    }
    else {
      const array = [{name: '--Choose One--', id: -1, dice: ''}];
      this.setState({gameModels: array.concat(this.state.gameModels)});
    }
  }

  onFailedSystemsSearch = (error) => {
    console.log(error);
    this.setState({gameModels: []});
  }
  onSuccessfulSystemsSearch = (result) => {
    let array = [{name: '--Choose One--', id: -1, dice: ''}];
    this.props.onGamemodelsChange(result);
    this.setState({gameModels: array.concat(result)});
  }

  onChoiceChange = (event) => {
    this.setState({chosenGameModel: event.target.value});
  }
  onNameChange = (event) => {
    this.setState({name: event.target.value});
  }
  onDescriptionChange = (event) => {
    this.setState({description: event.target.value});
  }
  onPrivateChange = (event) => {
    const {isPrivate} = this.state
    this.setState({isPrivate: !isPrivate});
  }
  onPlayersNumberChange = (event) => {
    this.setState({maxPlayers: event.target.value});
  }
  createRoom = () => {

  }

  render(){
    const {gameModels, name, description, isPrivate, maxPlayers, chosenGameModel} = this.state;
    return (
      <Form error>
        <Form.Field label='Choose a game system' control='select' onChange={this.onChoiceChange} value={chosenGameModel}>
          {
            gameModels.map(item=>
            <option key={item['name']} value={item['name']}>{item['name']} {item['dice']}</option>
            )
          }
        </Form.Field>
        <Form.Field>
          <label>How would you like your room to be named</label>
          <input type='text' placeholder='enter name' onChange={this.onNameChange} value={name}/>
        </Form.Field>
        <Form.Field>
          <label>Enter some kind of a description to your room so players know what will it be about</label>
          <input type='text' placeholder='enter description' onChange={this.onDescriptionChange} value={description}/>
        </Form.Field>
        <Form.Field>
          <label>How many players would you like to have in your group?</label>
          <input type='number' placeholder='1' onChange={this.onPlayersNumberChange} value={maxPlayers}/>
        </Form.Field>
        <Form.Field>
          <Checkbox label='Would you like your room to be private?' onClick={this.onPrivateChange} checked={isPrivate} toggle/>
        </Form.Field>
        <Button type='submit' onClick={this.createRoom}>Create your room</Button>
      </Form>
    );
  }

  componentDidMount() {
    this.getAvailableGameSystems();
  }
}

function mapStateToProps(state) {
  return {
    gamemodels: state.gamemodelsState,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onGamemodelsChange: (models) => dispatch(doChangeGamemodels(models)),
  };
}

const ConnectedRoomsAdder = connect(mapStateToProps, mapDispatchToProps)(RoomsAdder);


export default ConnectedRoomsAdder;
