import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Checkbox, Form, Message, Progress, Icon, Table } from 'semantic-ui-react';
import fetchFunction from '../../../helper_functions/fetcher';
import { Dimmer, Loader, Segment } from 'semantic-ui-react';
import {
  Link
} from 'react-router-dom';

class RoomsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      rooms: [],
      seekRoomName: '',
      seekOwner: '',
      seekDescription: '',
      seekTextId: '',
      loading: true
    }
    this.onFailedRoomsSearch = this.onFailedRoomsSearch.bind(this);
    this.onSuccessfulRoomsSearch = this.onSuccessfulRoomsSearch.bind(this);
    this.lookForRooms = this.lookForRooms.bind(this);
  }
  onFailedRoomsSearch(error) {
    console.log(error);
    this.setState({rooms: [], loading: false});
  }
  onSuccessfulRoomsSearch(result) {
    this.setState({rooms: result, loading: false});
  }
  lookForRooms(defaultSearch = true) {
    this.setState({loading: true});
    fetchFunction('rooms/', this.onSuccessfulRoomsSearch, this.onFailedRoomsSearch);
  }
  render() {
    const {rooms, seekRoomName, seekOwner, seekDescription, seekTextId, page, loading} = this.state;
    if (loading) {
      return (
      <Segment><br/><br/><br/><br/><br/>
        <Dimmer active inverted>
          <Loader size='massive'>Loading</Loader>
        </Dimmer>
      </Segment>
    );
    }
    else {
    return (
      <div>
      <Table celled structured>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Description</Table.HeaderCell>
            <Table.HeaderCell>System</Table.HeaderCell>
            <Table.HeaderCell>Owner</Table.HeaderCell>
            <Table.HeaderCell>Enter</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
        {
          rooms.map(item =>
          <Table.Row key={item['text_id']}>
            <Table.Cell>{item['name']}</Table.Cell>
            <Table.Cell>{item['description']}</Table.Cell>
            <Table.Cell>{item['game_model']}</Table.Cell>
            <Table.Cell>{item['owner']}</Table.Cell>
            <Table.Cell><a href={"/rooms/"+item['text_id']}>Click me</a></Table.Cell>
          </Table.Row>
        )
        }
        </Table.Body>
      </Table>
      <Link to={'/rooms/new'}>Create your room</Link>

    </div>
    );
  }
  }
  componentDidMount() {
    this.lookForRooms(true);
  }
}

export default RoomsList;
