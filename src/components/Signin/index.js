import { Button, Form, Message } from 'semantic-ui-react'
import React, { Component } from 'react';
import fetch from 'isomorphic-fetch';
import { Redirect } from 'react-router'
import { connect } from 'react-redux';
import {doChangeUserSessionToken} from '../../actions_creators/userSession';

//form needed to login
class SigninForm extends Component {
  constructor(props) {
    super(props);
    this.onTokenChange = props.onTokenChange;
    this.state = {
      username: '',
      password: '',
      login_errors: [],
      login_errors_exist: false,
      can_login: false,
    };
  }
  onPasswordChange = (event) => {
    this.setState({password: event.target.value});
    this.validateForm();
  }
  onUsernameChange = (event) => {
    this.setState({username: event.target.value});
    this.validateForm();
  }
  validateForm = () => {
    this.setState(prevState => {
      const {username,password} = prevState;
      const can_login = username.length > 5 && password.length > 5
      return {can_login: can_login};
    });
  }

  handleCriticalErrors = e => {
    this.setState({login_errors_exist: true, login_errors: {'internal_error': e.message}})
  }

  checkIfCorrectResponse = django_response => {
    const keys = Object.keys(django_response);
    if (keys.includes('token')) {
      this.onTokenChange(django_response['token']);
    }
    else {
      this.setState({login_errors_exist: true, login_errors: django_response});
    }
  }

  loginUser = (event) => {
    const {username, password} = this.state;
    const PATH_BASE = 'http://91.222.161.33:3000/login/';
    const FORM_PARAMS = {'username': username, 'password': password};
    fetch(PATH_BASE, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(FORM_PARAMS),
    })
  .then(response => response.json())
  .then(result => this.checkIfCorrectResponse(result))
  .catch(e => this.handleCriticalErrors(e));
  }
  render() {
    const {username, password, login_errors, login_errors_exist, can_login} = this.state;
    const token = this.props.user_session.token;
    if (token !== null) {
      return (
      <Redirect to={"/"}/> : null
      );
    }
      return (
      <Form error>
        <Form.Field>
          <label>Username</label>
          <input type='text' placeholder='Enter your username' value={username} onChange = {this.onUsernameChange}/>
        </Form.Field>
        <Form.Field>
          <label>Password</label>
          <input type='password' value={password} placeholder='Enter your password' onChange = {this.onPasswordChange}/>
        </Form.Field>
        { can_login === true ?  <Button type='submit' onClick={this.loginUser}>Sign in</Button> : <Button type='submit' onClick={this.loginUser} disabled>Sign in</Button> }
        {
          login_errors_exist ?
          <Message error>
            <Message.List>
              <Message.Header>Something went wrong:</Message.Header>
              {
                Object.keys(login_errors).map(item =>
                  <Message.Item>{login_errors[item]}</Message.Item>
                )
              }
            </Message.List>
          </Message>
          : null
        }

      </Form>
      );
    }

}
function mapStateToProps(state) {
  return {
    user_session: state.userSessionState,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onTokenChange: (token) => dispatch(doChangeUserSessionToken(token)),
  };
}

const ConnectedSigninForm = connect(mapStateToProps, mapDispatchToProps)(SigninForm);


export default ConnectedSigninForm;
