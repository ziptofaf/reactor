import { Button, Checkbox, Form, Message, Progress } from 'semantic-ui-react'
import React, { Component } from 'react';
import fetch from 'isomorphic-fetch';
import { Redirect } from 'react-router'
import { connect } from 'react-redux';
import {doChangeUserSessionToken} from '../../actions_creators/userSession';
import MAIN_PATH from '../../constants/urls';

const PasswordStrengthBar = ({strength, children}) => {
  if (strength === 0) {
    return (<Progress percent={10} error>Password is weak</Progress>);
  }
  if (strength === 1) {
    return (<Progress percent={50} warning>Password is average</Progress>);
  }
  if (strength >= 2) {
    return (<Progress percent={100} success>Password is strong</Progress>);
  }
}

//main signup form, complete with validations
class SignupForm extends Component {
  constructor(props) {
    super(props);
    this.onTokenChange = props.onTokenChange;
    this.state = {
      username: '',
      email: '',
      password: '',
      password_confirmation: '',
      agreed_to_terms: false,
      form_errors: [],
      password_strength: 0,
      registration_errors: {},
      registration_errors_exist: false,
    };
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onConfirmationChange = this.onConfirmationChange.bind(this);
    this.onAgreementChange = this.onAgreementChange.bind(this);
    this.validatePasswords = this.validatePasswords.bind(this);
    this.registerUser = this.registerUser.bind(this);
    this.onUsernameChange = this.onUsernameChange.bind(this);
    this.handleCriticalErrors = this.handleCriticalErrors.bind(this);
    this.checkIfCorrectResponse = this.checkIfCorrectResponse.bind(this);
  };

  checkIfCorrectResponse(django_response) {
    const keys = Object.keys(django_response);
    if (keys.includes('token')) {
      this.onTokenChange(django_response['token']);
    }
    else {
      this.setState({registration_errors_exist: true, registration_errors: django_response});
    }
  }

  handleCriticalErrors(e) {
    this.setState({registration_errors_exist: true, registration_errors: {'internal_error': e.message}})
  }
  validatePasswords(){
    this.setState(prevState => {
      const {username, email, password, password_confirmation} = prevState;
      let form_errors = [];
      let password_strength = 0;
      if (username.length < 5) {
        form_errors.push('Username is too short, it needs to be at least 5 characters long');
      }

      if (!email.match(/.@./)) {
        form_errors.push('This doesn\'t look like a valid email address');
      }
      if (password !== password_confirmation) {
        form_errors.push('Your password and password confirmation need to match.');
      }

      if (password.length < 6) {
        form_errors.push('Your password is too short');
      }
      if (password.length > 6)
      {
        if (password.match(/.?\d.?/)) {
          password_strength += 1;
        }
        if (password.match(/.?\W.?/)) {
          password_strength += 1;
        }
        if (password.length > 11) {
          password_strength += 1;
        }

      }
      return {form_errors: form_errors, password_strength: password_strength};
    });

  }

  onEmailChange(event) {
    this.setState({email: event.target.value});
    this.validatePasswords();
  }

  onConfirmationChange(event) {
    this.setState({password_confirmation: event.target.value});
    this.validatePasswords();
  }

  onAgreementChange(event) {
    this.setState({agreed_to_terms: !this.state.agreed_to_terms});
    this.validatePasswords();
  }

  onPasswordChange(event) {
    this.setState({password: event.target.value});
    this.validatePasswords();
  }
  onUsernameChange(event) {
    this.setState({username: event.target.value});
    this.validatePasswords();
  }
  registerUser(event) {
    const {username, email, password, password_confirmation} = this.state;
    const PATH_BASE = MAIN_PATH + 'register/';
    const FORM_PARAMS = {'username': username, 'email': email, 'password1': password, 'password2': password_confirmation};
    fetch(PATH_BASE, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(FORM_PARAMS),
    })
  .then(response => response.json())
  .then(result => this.checkIfCorrectResponse(result))
  .catch(e => this.handleCriticalErrors(e));
  }

  render() {
    const {password, password_confirmation,
      agreed_to_terms, registration_errors_exist, registration_errors,
      form_errors, password_strength} = this.state;
      const token = this.props.user_session.token;
  if (token !== null) {
    return (
    <Redirect to={"/"}/> : null
    );
  }
  return (
  <Form error>
    <Form.Field>
      <label>Username</label>
      <input type='text' placeholder='Enter your desired username' onChange = {this.onUsernameChange}/>
    </Form.Field>
    <Form.Field>
      <label>Email</label>
      <input type='email' placeholder='Enter your email' onChange = {this.onEmailChange}/>
    </Form.Field>
    <Form.Field>
      <label>Password</label>
      <input type='password' value={password} placeholder='Enter your password' onChange = {this.onPasswordChange}/>
    </Form.Field>
    <Form.Field>
      <label>Password confirmation</label>
      <input type='password' value={password_confirmation} placeholder='Enter your password again' onChange={this.onConfirmationChange}/>
    </Form.Field>
    <PasswordStrengthBar strength={password_strength}></PasswordStrengthBar>
    {
      form_errors.length > 0 ?
      <Message error>
          <Message.List>
            <Message.Header>Following errors prevent you from registering:</Message.Header>
          {
            form_errors.map(item =>
              <Message.Item key={item}>{item}</Message.Item>
            )
          }
        </Message.List>
      </Message>
      : null
    }
    <Form.Field>
      <Checkbox label='I agree to the Terms and Conditions' onClick={this.onAgreementChange}/>
    </Form.Field>
    {
      (agreed_to_terms && form_errors.length === 0) ? <Button type='submit' onClick={this.registerUser}>Submit</Button> : <Button type='submit' disabled>Submit</Button>
    }
    {
      registration_errors_exist ?
      <Message error>
        <Message.List>
          <Message.Header>Following errors are preventing you from registering:</Message.Header>
          {
            Object.keys(registration_errors).map(item =>
              <Message.Item key={item}>{registration_errors[item]}</Message.Item>
            )
          }
        </Message.List>
      </Message>
      : null
    }
  </Form>
  );
  }

}

function mapStateToProps(state) {
  return {
    user_session: state.userSessionState,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onTokenChange: (token) => dispatch(doChangeUserSessionToken(token)),
  };
}

const ConnectedSignupForm = connect(mapStateToProps, mapDispatchToProps)(SignupForm);


export default ConnectedSignupForm;
