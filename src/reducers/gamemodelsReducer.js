import {GAMEMODELS_CHANGE} from '../constants/actionTypes';

const game_models = {gamemodels: []};

function gamemodelsReducer(state = game_models, action) {
  switch(action.type) {
    case GAMEMODELS_CHANGE : {
      return gamemodelsChange(state, action);
    }

  default : return state;
  }
}

function gamemodelsChange(state, action) {
  const gamemodels_list = action.available_game_models;
  return {gamemodels: gamemodels_list};
}

export default gamemodelsReducer;
