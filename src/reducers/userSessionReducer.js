import {USER_SESSION_CHANGE} from '../constants/actionTypes';

const user_session = {token: null};

function userSessionReducer(state = user_session, action) {
  switch(action.type) {
    case USER_SESSION_CHANGE : {
      return userSessionChange(state, action);
    }

  default : return state;
  }
}

function userSessionChange(state, action) {
  const new_user_session = action.user_session.token;
  let expiration = new Date(); //1 hour from now;
  expiration.setMinutes(expiration.getMinutes() + 10);
  return {token: new_user_session, expires_at: expiration};
}

export default userSessionReducer;
