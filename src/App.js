import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import {Button, Container, Header, Menu} from 'semantic-ui-react';
//import './App.css';
import {Home} from './components/Home';
import {Signup} from './components/Signup';
import Signin from './components/Signin';
import SessionManager from './components/SessionManager';
import Signout from './components/Signout';
import RoomsList from './components/Rooms/List';
import RoomsAdder from './components/Rooms/Add';

import { connect } from 'react-redux';

class App extends Component {
  menuBasedOnRegistration = () => {
    if (this.props.user_session.token !== null)
    {
      return [
        {'url':'/rooms', 'display': 'Rooms'},
        {'url':'/sign-out', 'display': 'Sign out'},
      ];
    }
    else {
      return [
        {'url':'/rooms', 'display': 'Rooms'},
        {'url':'/sign-in', 'display': 'Sign in'},
        {'url':'/sign-up', 'display': 'Sign up'},
      ];
    }
  }

  render() {
    const login_fields = this.menuBasedOnRegistration();
    return (
          <Router>
            <Container>
              <Menu>
                <Menu.Item><Link to="/">Home</Link></Menu.Item>
                {
                  login_fields.map(item =>
                    <Menu.Item key={item['display']}><Link to={item['url']}>{item['display']}</Link></Menu.Item>
                  )
                }
              </Menu>
              <Route exact path="/" component={Home} />
              <Route path="/sign-up" component={Signup} />
              <Route path ='/sign-in' component={Signin} />
              <Route path='/sign-out' component={Signout} />
              <Route exact path='/rooms/new' component={RoomsAdder} />
              <Route exact path='/rooms' component={RoomsList} />
              <SessionManager/>
            </Container>
          </Router>
        );
  }
}

function mapStateToProps(state) {
  return {
    user_session: state.userSessionState,
  };
}
const ConnectedApp = connect(mapStateToProps)(App);

export default ConnectedApp;
