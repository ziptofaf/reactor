import {GAMEMODELS_CHANGE} from '../constants/actionTypes';

function doChangeGamemodels(gamemodels) {
  return {
    type: GAMEMODELS_CHANGE,
    available_game_models: gamemodels,
  };
}

export {doChangeGamemodels};
