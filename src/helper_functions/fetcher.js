import fetch from 'isomorphic-fetch';
import {store} from '../store';
import MAIN_PATH from '../constants/urls';
const PATH_BASE = MAIN_PATH;
//we use this function to provide somewhat unified interface for fetch management
const fetchResourceFromBackEnd = (relative_path, function_if_correct, function_if_fail, params={}, http_verb='GET') => {
  if (relative_path === undefined) {return;}
  var states = store.getState();
  let token = null;
  if ('userSessionState' in states && states.userSessionState.token) {
    token = states.userSessionState.token;
  }
  let headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  };
  if (token !== null) {
    const token_header = "JWT " + token;
    headers['Authorization'] = token_header;
  }
  if (http_verb === 'GET')
  {
    let stringedParams = "";
    const keys = Object.keys(params);
    keys.forEach( (key) => {
    stringedParams += key + '=' + params[key] + '&'
    });
    fetch(PATH_BASE + relative_path + '?' + stringedParams, {
      method: http_verb,
      headers: headers,
    })
    .then(response => response.json())
    .then(result => function_if_correct(result))
    .catch(e => function_if_fail(e));
  }
  else {
    fetch(PATH_BASE + relative_path, {
      method: http_verb,
      headers: headers,
      body: JSON.stringify(params),
    })
    .then(response => response.json())
    .then(result => function_if_correct(result))
    .catch(e => function_if_fail(e));
  }
  }

store.subscribe(fetchResourceFromBackEnd);

export default fetchResourceFromBackEnd;
