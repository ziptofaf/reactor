import { applyMiddleware, combineReducers, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native
import logger from 'redux-logger';


import userSessionReducer from '../reducers/userSessionReducer';
import gamemodelsReducer from '../reducers/gamemodelsReducer';

const rootReducer = combineReducers({
  userSessionState: userSessionReducer,
  gamemodelsState: gamemodelsReducer,
});

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['userSessionState']
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, undefined, applyMiddleware(logger));
let persistor = persistStore(store);
export {store, persistor};
